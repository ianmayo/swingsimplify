import java.util.Arrays;

public class PathUtils {
    public static final double EARTH_RADIUS = 6371000.0;

    /**
     *
     * It's iterative version of Douglas-Peucker algorithm (http://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)
     * Current implementation generalizes wiki implementation introducing "baseline" and "point metric" terms:
     *
     *  * baseline - the figure between two points which represents the shortest path from start point to end point. (arc for geo metrics and line for plane metrics)
     *  * point metric  - the shortest distance between specified point and baseline which approximates the point
     *
     * Wiki algorithm uses plane metrics by default ("line" is line between start and end point ("baseline"),
     *                              "perpendicular distance" - the shortest path between current point and the line ("point metric"))
     *
     * for i = 2 to (length(PointList) - 1)
     *  d = PerpendicularDistance(PointList[i], Line(PointList[1], PointList[end]))
     *  if d > dmax
     *    index = i
     *    dmax = d
     *  end
     * end|
     *
     * our equialent is:
     *
     * metrics.setCurrentBaseline(PointList[1], PointList[end]);
     * for i = 2 to (length(PointList) - 1)
     *  d = metrics.getPointMetric(PointList[i]);
     *  if d > dmax
     *    index = i
     *    dmax = d
     *  end
     * end
     *
     */
    private static int[] simplify(double []x, double []y, int pointsCount, Metrics metrics) {
        if (pointsCount >= x.length) {
            int[] result = new int[x.length];
            for (int i = 0; i < x.length; i++) {
                result[i] = 0;
            }
        }
        int[] points = new int[pointsCount + 1];
        points[0] = 0;
        points[1] = x.length - 1;
        int size = 2;
        while (size != pointsCount) {
            double maxDistance = 0;
            int maxIndex = -1;
            int maxFirst = -1;
            for (int i = 0; i < size - 1; i++) {
                int first = points[i];
                int last = points[i + 1];
                metrics.setCurrentBaseline(x[first], x[last], y[first], y[last]);
                for (int j = first + 1; j < last; j++) {
                    double distance = metrics.getPointMetric(x[j], y[j]);
                    if (distance > maxDistance) {
                        maxDistance = distance;
                        maxIndex = j;
                        maxFirst = i;
                    }
                }
            }
            if (maxFirst == -1) {
                break;
            }
            for (int i = size; i > maxFirst; i--) {
                points[i] = points[i - 1];
            }
            points[maxFirst + 1] = maxIndex;
            size++;
        }
        return Arrays.copyOf(points, size);
    }

    public static int[] simplifyGeoPath(double[] lat, double[] lon, int pointsCount) {
        return simplify(lat, lon, pointsCount, new GeoMetrics());
    }

    public static int[] simplifyTimedData(double[] time, double[] value, int pointsCount) {
        return simplify(time, value, pointsCount, new PlaneMetrics());
    }

    private static interface Metrics {
        void setCurrentBaseline(double xStart, double xEnd, double yStart, double yEnd);

        double getPointMetric(double x, double y);
    }

    private static class GeoMetrics implements Metrics {
        private static final int X = 0;
        private static final int Y = 1;
        private static final int Z = 2;

        double[] startCoords;
        double[] endCoords;
        double planeB;
        double planeA;
        double distaceBetweenStartEnd;

        private double[] toXYZ(double lat, double lon) {
            lat = Math.toRadians(lat);
            lon = Math.toRadians(lon);
            double[] result = new double[3];
            result[0] = EARTH_RADIUS * Math.cos(lat) * Math.cos(lon);
            result[1] = EARTH_RADIUS * Math.cos(lat) * Math.sin(lon);
            result[2] = EARTH_RADIUS * Math.sin(lat);
            return result;
        }

        private double distanceBetweenPoints(double[] point1, double[] point2) {
            return Math.sqrt((point1[X] - point2[X]) * (point1[X] - point2[X]) +
                    (point1[Y] - point2[Y]) * (point1[Y] - point2[Y]) +
                    (point1[Z] - point2[Z]) * (point1[Z] - point2[Z])
            );
        }

        /**
         * baseline in geo metrics is arc of circle with radius of earth and center in the center of earth
         * between (latStart; lonStart) and (latEnd; lonEnd) points
         *
         * find equation of plane which contains center of earth and this arc:
         *   z(y, x) = planeA * x + planeB * y.
         */
        public void setCurrentBaseline(double latStart, double latEnd, double lonStart, double lonEnd) {
            startCoords = toXYZ(latStart, lonStart);
            endCoords = toXYZ(latEnd, lonEnd);

            planeB = (startCoords[Z] * (startCoords[X] + endCoords[X]) - startCoords[X] * (startCoords[Z] + endCoords[Z])) /
                (startCoords[Y] * (startCoords[X] + endCoords[X]) - startCoords[X] * (startCoords[Y] + endCoords[Y]));
            planeA = (startCoords[Z] + endCoords[Z] - planeB * (startCoords[Y] + endCoords[Y])) / (startCoords[X] + endCoords[X]);
            distaceBetweenStartEnd = distanceBetweenPoints(startCoords, endCoords);
        }

        /**
         * the shortest distance in geo metrics is the shortest distance from point to base arc
         * 1. find projection of current point on plane which contains center of earth and base arc
         * 2. find the nearest point on the arc to this projection on plane which contains center of earth and base arc
         * 3. the result is a distance from current point to the nearest point on the arc that is found on step 2
         */
        public double getPointMetric(double lat, double lon) {
            double[] pointCoords = toXYZ(lat, lon);
            double[] pointCoordsPlane = new double[3];
            double[] minPoint;

            double t = (pointCoords[Z] - planeA * pointCoords[X] - planeB * pointCoords[Y]) / (planeA * planeA + planeB * planeB + 1);
            pointCoordsPlane[X] = pointCoords[X] + planeA * t;
            pointCoordsPlane[Y] = pointCoords[Y] + planeB * t;
            pointCoordsPlane[Z] = pointCoords[Z] - t;

            double[] pointOnArc = new double[3];
            t = Math.sqrt((EARTH_RADIUS / ((pointCoordsPlane[X] * pointCoordsPlane[X]) +
                    (pointCoordsPlane[Y] * pointCoordsPlane[Y]) + (pointCoordsPlane[Z] * pointCoordsPlane[Z]))) * EARTH_RADIUS);
            pointOnArc[X] = t * pointCoordsPlane[X];
            pointOnArc[Y] = t * pointCoordsPlane[Y];
            pointOnArc[Z] = t * pointCoordsPlane[Z];

            if (distaceBetweenStartEnd > distanceBetweenPoints(startCoords, pointOnArc) &&
                    distaceBetweenStartEnd > distanceBetweenPoints(endCoords, pointOnArc)) {
                minPoint = pointOnArc;
            } else {
                if (distanceBetweenPoints(pointCoordsPlane, startCoords) > distanceBetweenPoints(pointCoordsPlane, endCoords)) {
                    minPoint = endCoords;
                } else {
                    minPoint = startCoords;
                }
            }
            return distanceBetweenPoints(pointCoords, minPoint);
        }
    }

    private static class PlaneMetrics implements Metrics {
        double a;
        double b;

        /**
         * the shortest path in plain metrics is perpendicular from current point on baseline
         */
        public double getPointMetric(double x, double y) {
            return Math.abs(a * x - y + b) / Math.sqrt(a * a + 1);
        }

        /**
         * baseline in plain metrics is line y(x) = a * x + b;
         */
        public void setCurrentBaseline(double xStart, double xEnd, double yStart, double yEnd) {
            a = (yStart - yEnd) / (xStart - xEnd);
            b = yStart - a * xStart;
        }
    }
}
